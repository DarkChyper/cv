<?php
session_start();
$autoloader = function ($full_class_name) {

    $name = str_replace('\\', DIRECTORY_SEPARATOR, $full_class_name);
    $path = __DIR__ . DIRECTORY_SEPARATOR . $name . '.php';

    if (is_file($path)) {
        include $path;
        return true;
    } else {
        return false;
    }
};

spl_autoload_register($autoloader);

use AppBundle\Entity\Page;

function defineBreadCrumb(Page $page, $actif = true)
{
    $retour = "";

    if ($page->getParent() != null) {
        $retour .= defineBreadCrumb($page->getParent(), false);
    }

    if($actif){
        $retour .= '<li class="breadcrumb-item active">' . $page->getTitle() . '</li>';
    } else {
        $retour .= '<li class="breadcrumb-item"><a href="?p=' . $page->getUrl() . '">' . $page->getTitle() . '</a></li>';
    }
    return $retour;
}


// definition des pages valides
$pAccueil = new Page('CV', 'accueil', './pages/accueil.php');
$pParcoursCDP = new Page('Parcours Chef de Projet', 'cdp', './pages/formation_cdp.php', $pAccueil);


// mise en place des pages valides dans un tableau pour faciliter la vérification
$pagesOK = array(
    $pAccueil->getUrl() => $pAccueil,
    $pParcoursCDP->getUrl() => $pParcoursCDP,
);

if (isset($_GET['p']) && isset($pagesOK[$_GET['p']])) {
    $_SESSION['pageActuelle'] = $pagesOK[$_GET['p']];
} else {
    $_SESSION['pageActuelle'] = $pagesOK['accueil'];
}

?>
<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="utf-8"/>
    <meta http-equiv="x-ua-compatible" content="ie=edge"/>
    <meta name="viewport" content="width=device-width, initial-scale=1"/>

    <title><?php echo $_SESSION['pageActuelle']->getTitle(); ?> - SIMON LHOIR</title>

    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css"
          integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <link rel="stylesheet" href="assets/css/style.css">
    <!-- javascript -->
    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js" integrity="sha384-OgVRvuATP1z7JjHLkuOU7Xw704+h835Lr+6QL9UvYjZE3Ipu6Tp75j7Bh/kR0JKI" crossorigin="anonymous"></script>
    <script src="https://kit.fontawesome.com/a91a27e46f.js" crossorigin="anonymous"></script>
    <link rel="icon" href="simonlhoir.ico"/>
</head>
<body>
<div id="page" class="container">
    <div class="row">
        <div class="col-12">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <?php echo defineBreadCrumb($_SESSION['pageActuelle']); ?>
                </ol>
            </nav>
        </div>
    </div>
    <?php
    include($_SESSION['pageActuelle']->getPath());
    ?>

</div>

<?php
include('./pages/common/bottom_nav.php');
?>

</body>
</html>