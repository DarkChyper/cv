<div class="row">

    <!-- EXPERIENCE -->
    <div id="experience" class="col-12 col-md-6">
        <h3 class="title-experience">EXPERIENCE</h3>
        <div class="card border-light">
            <div class="card-body">
                <a class="not-decorated collapsed" data-toggle="collapse" href="#collapseTeranga" role="button"
                   aria-expanded="false" aria-controls="collapseTeranga">
                    <h5 class="card-title"><span class="accicon"><i class="fas fa-angle-down rotate-icon"></i></span>&nbsp;06/2023 - aujourd'hui | Ingénieur d’études PHP | Teranga</h5>
                    <h6 class="card-subtitle mb-1 text-muted">Poste en full remote</h6>
                </a>
                <div class="collapse" id="collapseTeranga">
                    <p class="card-text">Développement PHP avec framework interne sur les outils (NETPlanning) de suivis des temps et absences des intervenants dans le milieu du médico-sociale (EHPAD, ESAT…)<br />Découverte de VueJS avec une PWA basée sur Quasar</p>
                </div>
                <p>
                    <span class="badge badge-success">PHP 8.3</span>
                    <span class="badge badge-primary">VueJS 2</span>
                </p>

            </div>
        </div>
        <div class="card border-light">
            <div class="card-body">
                <a class="not-decorated collapsed" data-toggle="collapse" href="#collapseOsConcept" role="button"
                   aria-expanded="false" aria-controls="collapseOsConcept">
                    <h5 class="card-title"><span class="accicon"><i class="fas fa-angle-down rotate-icon"></i></span>&nbsp;09/2020 - 06/2023 | Ingénieur d’études PHP Symfony | OSConcept</h5>
                    <h6 class="card-subtitle mb-1 text-muted">Développement produit en mode saas</h6>
                </a>
                <div class="collapse" id="collapseOsConcept">
                    <p class="card-text">Evolutions techniques et fonctionnelles de l'application oHRis, logiciel de
                        SIRH, produit par la société en mode saas.<br/>
                        Développement web dans un contexte mixte entre la fonction publique et le privé.</p>
                    <p class="card-text">
                    <ul>
                        <li>Création de la fonctionnalité "Don de jours", adaptatif entre les réglementations de la
                            fonction publique et du privé
                        </li>
                        <li>Création de la fonctionnalité "Télétravail"</li>
                        <li>Travail sur la gestion des temps, affichage d'un suivis de présence interactif, des modèles
                            de données applicatives
                        </li>
                        <li>Refonte de l'API avec API Platform (2.6) anciennement avec FOSRestBundle</li>
                        <li>Participation aux créations et maintenance des connecteurs standards et spécifiques</li>
                    </ul>
                    </p>
                </div>
                <p>
                    <span class="badge badge-success">PHP 7.4 - 8.0</span>
                    <span class="badge badge-success">Symfony 4 et 5</span>
                    <span class="badge badge-primary">API Platform</span>
                    <span class="badge badge-secondary">MySQL</span>
                    <span class="badge badge-secondary">Test Panther</span>
                </p>

            </div>
        </div>


        <div class="card border-light">
            <div class="card-body">
                <a class="not-decorated collapsed" data-toggle="collapse" href="#collapseAusy" role="button"
                   aria-expanded="false" aria-controls="collapseAusy">
                    <h5 class="card-title"><span class="accicon"><i class="fas fa-angle-down rotate-icon"></i></span>&nbsp;11/2019-03/2020 | Ingénieur d’études JAVA | AUSY</h5>
                    <h6 class="card-subtitle mb-1 text-muted">En mission chez Décathlon (Croix)</h6>
                </a>
                <div class="collapse" id="collapseAusy">
                    <p class="card-text">Maintenance et développement d’applications de transport et de douane dans une
                        squad de 5 développeurs.</p>
                </div>
                <p>
                    <span class="badge badge-success">JDK8</span>
                    <span class="badge badge-success">Struts 1.2</span>
                    <span class="badge badge-secondary">Tomcat 9</span>
                    <span class="badge badge-secondary">Oracle/Postgres</span>
                </p>

            </div>
        </div>

        <div class="card border-light">
            <div class="card-body">
                <a class="not-decorated collapsed" data-toggle="collapse" href="#collapseSopra" role="button"
                   aria-expanded="false" aria-controls="collapseSopra">
                    <h5 class="card-title"><span class="accicon"><i class="fas fa-angle-down rotate-icon"></i></span>&nbsp;10/2016-10/2019 | Ingénieur d’études JAVA | Sopra Steria
                    </h5>
                    <h6 class="card-subtitle mb-1 text-muted">En mission chez AG2R La Mondiale (Mons-en-Baroeul)</h6>
                </a>
                <div class="collapse" id="collapseSopra">
                    <p class="card-text">
                    <ul>
                        <li>Migration technique du parc applicatif Java.</li>
                        <li>Refonte d’application historique en java avec le framework client (GWT) pour communiquer
                            avec le
                            mainframe.
                        </li>
                        <li>Maintenance, évolution et transformation de projet en méthodologies agiles.</li>

                    </ul>
                    </p>
                </div>
                <p>
                    <span class="badge badge-success">JDK5-8</span>
                    <span class="badge badge-success">JSF/Facet</span>
                    <span class="badge badge-success">GWT</span>
                    <span class="badge badge-success">Spring Batch</span>
                    <span class="badge badge-secondary">Selenium</span>
                    <span class="badge badge-warning">Gitlab</span>
                    <span class="badge badge-warning">Cloubees/Sonar</span>
                </p>

            </div>
        </div>

        <div class="card border-light">
            <div class="card-body">
                <a class="not-decorated collapsed" data-toggle="collapse" href="#collapseGfi" role="button"
                   aria-expanded="false" aria-controls="collapseGfi">
                    <h5 class="card-title"><span class="accicon"><i class="fas fa-angle-down rotate-icon"></i></span>&nbsp;05/2015–09/2016 | Concepteur developpeur NTIC | Gfi

                    </h5>
                    <h6 class="card-subtitle mb-1 text-muted">En mission pour BNPParisBas (Lille)</h6>
                </a>
                <div class="collapse" id="collapseGfi">
                    <p class="card-text">Suivis de production et développement Java sur le progiciel de saisie
                        comptable.
                    </p>
                </div>
                <p>
                    <span class="badge badge-success">JDK7</span>
                    <span class="badge badge-info">Batch windows</span>
                    <span class="badge badge-info">Shell</span>
                    <span class="badge badge-warning">GIT</span>
                    <span class="badge badge-warning">Jenkins/Sonar</span>
                </p>

            </div>
        </div>

        <div class="card border-light">
            <div class="card-body">
                <a class="not-decorated collapsed" data-toggle="collapse" href="#collapseActif" role="button"
                   aria-expanded="false" aria-controls="collapseActif">
                    <h5 class="card-title"><span class="accicon"><i class="fas fa-angle-down rotate-icon"></i></span>&nbsp;09/2014–04/2015 | Analyste SQL/UNIX | Actif Solution</h5>
                    <h6 class="card-subtitle mb-1 text-muted">En mission pour BNPParisBas (Lille)</h6>
                </a>
                <div class="collapse" id="collapseActif">
                    <p class="card-text">suivis de production, analyse d’incidents, contournements « à chaud » puis
                        correctifs « à froid ». Automatisation des tâches quotidiennes en Bash.
                    </p>
                </div>
                <p>
                    <span class="badge badge-info">Batch windows</span>
                    <span class="badge badge-info">Shell</span>

                </p>
            </div>
        </div>

        <div class="card border-light">
            <div class="card-body">
                <a class="not-decorated collapsed" data-toggle="collapse" href="#collapseAnkama" role="button"
                   aria-expanded="false" aria-controls="collapseAnkama">
                    <h5 class="card-title"><span class="accicon"><i class="fas fa-angle-down rotate-icon"></i></span>&nbsp;03/2010–02/2014 | Truquiste | Ankama </h5>
                    <h6 class="card-subtitle mb-1 text-muted">Equipe Production Audiovisuelle</h6>
                </a>
                <div class="collapse" id="collapseAnkama">
                    <p class="card-text">Création de scripts C# se basant sur l’API d’un logiciel de montage.
                        Rendus automatisés et déplacements de fichiers sur le réseau windows de l’entreprise.</p>
                </div>
                <p>
                    <span class="badge badge-success">C#</span>

                </p>
            </div>
        </div>

    </div>
    <!-- FIN EXPERIENCE -->

    <!-- COMPETENCES -->
    <div id="competences" class="col-12 col-md-6">
        <h3 class="title-competences">COMPETENCES</h3>
        <p><span class="competence-sub-title">Méthodologies</span>&nbsp;Cycle en V, Agilité : SCRUM & KANBAN</p>
        <p><span class="competence-sub-title">Architecture</span>&nbsp;MVC<small>(3)</small>, Webservices <small>(3)</small>: REST & SOAP</p>
        <p><span class="competence-sub-title">Langages</span>&nbsp;PHP<small>(2)</small>, Ant<small>(2)</small>, Shell<small>(2)</small>, CSS<small>(1)</small>, HTML<small>(3)</small> Js/TypeScript<small>(1)</small>, Python<small>(1)</small> </p>
        <p><span class="competence-sub-title">Frameworks</span>&nbsp;Symfony<small>(2)</small>, Angular 8<small>(1)</small></p>
        <p><span class="competence-sub-title">Intégration continue</span>&nbsp;Jenkins<small>(2)</small>, Sonar<small>(2)</small>, GIT<small>(2)</small>: Lab, Hub</p>
        <p><span class="competence-sub-title">Outils/Middleware</span>&nbsp;PHPStorm</p>
        <p><span class="competence-sub-title">SGBD</span>&nbsp;Oracle<small>(2)</small>, MySQL<small>(2)</small>, PostgreSQL<small>(2)</small></p>
        <p><span class="competence-sub-title">Serveurs</span>&nbsp;Tomcat<small>(2)</small>, Apache<small>(3)</small></p>
        <p><span class="competence-sub-title">Systèmes</span>&nbsp;Unix, OSx, Windows</p>
        <p><span class="competence-sub-title">Langues</span>&nbsp;Anglais ~B2</p>
        <p><small>(1) junior (2) confirmé (3) expert</small></p>
    </div>
    <!-- COMPETENCES -->


