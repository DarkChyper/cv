<?php

namespace AppBundle\Entity;

class Page
{
    protected $title;
    protected $url;
    protected $path;
    protected $parent;

    /**
     * Page constructor.
     * @param $title
     * @param $url
     * @param $path
     * @param $parent
     */
    public function __construct($title, $url, $path, Page $parent = null)
    {
        $this->title = $title;
        $this->url = $url;
        $this->path = $path;
        $this->parent = $parent;
    }


    /**
     * @return mixed
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param mixed $title
     */
    public function setTitle($title)
    {
        $this->title = $title;
    }

    /**
     * @return mixed
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * @param mixed $url
     */
    public function setUrl($url)
    {
        $this->url = $url;
    }

    /**
     * @return mixed
     */
    public function getPath()
    {
        return $this->path;
    }

    /**
     * @param mixed $path
     */
    public function setPath($path)
    {
        $this->path = $path;
    }

    /**
     * @return mixed
     */
    public function getParent()
    {
        return $this->parent;
    }

    /**
     * @param mixed $parent
     */
    public function setParent(Page $parent)
    {
        $this->parent = $parent;
    }


}