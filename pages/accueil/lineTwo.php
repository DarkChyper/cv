<div class="row">

    <!-- FORMATIONS -->
    <div id="formations" class="col-12 col-md-6">
        <h3 class="title-formation">FORMATION</h3>

        <div class="card border-light">
            <div class="card-body">
                <h5 class="card-title">2017 - Maîtrise Chef de projet multimédia</h5>
                <h6 class="card-subtitle mb-1 text-muted">IESA - Openclassrooms</h6>
                <p class="card-text">Gestion de projet et d'équipe, cahier des charges, planning. Développement web,
                    symfony et iOS</p>

                <a href="?p=cdp" class="card-link">En savoir plus</a>
            </div>
        </div>

        <div class="card border-light">
            <div class="card-body">
                <h5 class="card-title">2014 à aujourd'hui - certifications diverses</h5>
                <h6 class="card-subtitle mb-1 text-muted">Coursera - Openclassrooms</h6>
                <p class="card-text">Git, html-css-js-php, Bootstrap, DOM JS, Python, Java, Node.JS, Angular</p>

                <a href="https://www.linkedin.com/in/simonlhoir/" class="card-link" target="_BLANK">linkedin</a>
            </div>
        </div>


        <div class="card border-light">
            <div class="card-body">
                <h5 class="card-title">2014 - certification MainFrame Cobol</h5>
                <h6 class="card-subtitle mb-1 text-muted">Actif Solution</h6>
            </div>
        </div>

        <div class="card border-light">
            <div class="card-body">
                <h5 class="card-title">2010 - Master Audiovisuel et Multimédia TRUCIS</h5>
                <h6 class="card-subtitle mb-1 text-muted">UVHC - DREAM</h6>
                <p class="card-text">Préparation de tournage, réalisation, montage, post-production image et son et
                    diffusion. Culture de l'image et du son.</p>
            </div>
        </div>

    </div>
    <!-- FIN FORMATIONS -->

    <!-- PROJETS -->
    <div id="projets" class="col-12 col-md-6">
        <h3 class="title-projets">PROJETS</h3>
        <div class="card border-light">
            <div class="card-body">
                <a class="not-decorated collapsed" data-toggle="collapse" href="#collapseTempo" role="button"
                   aria-expanded="false" aria-controls="collapseTempo">
                    <h5 class="card-title"><span class="accicon"><i class="fas fa-angle-down rotate-icon"></i></span>&nbsp;2023
                        | Tempo</h5>
                    <h6 class="card-subtitle mb-1 text-muted">Projet personnel</h6>
                </a>
                <div class="collapse" id="collapseTempo">
                    <p class="card-text">Un site pour récupérer les information TEMPO de EF via les API de RTE.<br />L'idée est de transmettre ces informations vers différents canaux, tels que les push web, le css ou un calendrier ics.
                    </p>
                </div>
                <p>
                    <span class="badge badge-success">PHP 8</span>
                    <span class="badge badge-success">Symfony 6</span>
                </p>
                <a href="https://tempo.lhoir.me" target="_BLANK" class="card-link">Le projet Tempo</a>
            </div>
        </div>
        <div class="card border-light">
            <div class="card-body">
                <a class="not-decorated collapsed" data-toggle="collapse" href="#collapseDarkBlog" role="button"
                   aria-expanded="false" aria-controls="collapseDarkBlog">
                    <h5 class="card-title"><span class="accicon"><i class="fas fa-angle-down rotate-icon"></i></span>&nbsp;2023
                        | DarkBlog</h5>
                    <h6 class="card-subtitle mb-1 text-muted">Projet personnel</h6>
                </a>
                <div class="collapse" id="collapseDarkBlog">
                    <p class="card-text">Création d'un moteur de pages statique pour un blog personnel.<br />Le blog est basé sur le protocole Gémini, et exporte également les pages au format HTML avec un css et js très léger.
                    </p>
                </div>
                <p>
                    <span class="badge badge-success">PHP 8</span>
                </p>
                <a href="https://darkblog.lhoir.me" target="_BLANK" class="card-link">Lien du blog</a>
            </div>
        </div>
        <div class="card border-light">
            <div class="card-body">
                <a class="not-decorated collapsed" data-toggle="collapse" href="#collapseDDOManager" role="button"
                   aria-expanded="false" aria-controls="collapseDDOManager">
                    <h5 class="card-title"><span class="accicon"><i class="fas fa-angle-down rotate-icon"></i></span>&nbsp;2022
                        | DDOManager</h5>
                    <h6 class="card-subtitle mb-1 text-muted">Projet personnel</h6>
                </a>
                <div class="collapse" id="collapseDDOManager">
                    <p class="card-text">DDOManager est un petit outil permettant de mettre à jour les DNS OVH via leur
                        API avec son IP dynamique à la maison.<br/>Utile pour auto-héberger des projets comme un blog.
                    </p>
                </div>
                <p>
                    <span class="badge badge-success">PHP 8</span>
                    <span class="badge badge-success">Symfony 5</span>
                    <span class="badge badge-success">API REST</span>
                    <span class="badge badge-primary">OVH</span>
                </p>
                <a href="https://github.com/DarkChyper/DDOManager" target="_BLANK" class="card-link">Code source</a>
            </div>
        </div>

        <div class="card border-light">
            <div class="card-body">
                <a class="not-decorated collapsed" data-toggle="collapse" href="#collapseBB" role="button"
                   aria-expanded="false" aria-controls="collapseBB">
                    <h5 class="card-title"><span class="accicon"><i class="fas fa-angle-down rotate-icon"></i></span>&nbsp;12/2019-2020
                        | Projet BB</h5>
                    <h6 class="card-subtitle mb-1 text-muted">Projet personnel</h6>
                </a>
                <div class="collapse" id="collapseBB">
                    <p class="card-text">J’ai créé un site web à destination de la famille et des amis, afin de rendre
                        ludique l’arrivée d’un bébé.<br/>
                        Les utilisateurs peuvent pronostiquer sur le sexe du bébé puis sur le prénom une fois le sexe
                        révélé.</p>
                </div>
                <p>
                    <span class="badge badge-success">JDK8</span>
                    <span class="badge badge-success">Spring Boot</span>
                    <span class="badge badge-success">API REST</span>
                    <span class="badge badge-primary">Angular 8</span>
                    <span class="badge badge-secondary">SSL</span>
                    <span class="badge badge-secondary">Raspberry Pi</span>
                </p>
                <a href="https://gitlab.com/baby-project" target="_BLANK" class="card-link">Code source</a>
                <a href="https://bb.lhoir.me" target="_blank" class="card-link">Site web</a>
            </div>
        </div>

        <div class="card border-light">
            <div class="card-body">
                <a class="not-decorated collapsed" data-toggle="collapse" href="#collapseDomotique" role="button"
                   aria-expanded="false" aria-controls="collapseDomotique">
                    <h5 class="card-title"><i
                                class="fas fa-angle-down rotate-icon"></i></span>&nbsp;2017-<?php $now = new \DateTime();
                        echo $now->format("Y"); ?> |
                        Domotique</h5>
                    <h6 class="card-subtitle mb-1 text-muted">Projet personnel</h6>
                </a>
                <div class="collapse" id="collapseDomotique">
                    <p class="card-text">J’utilise des raspberry pi et des arduino comme collecteurs de données (photos,
                        température, humidité...) des différentes pièces de mon habitation.<br/>Un autre raspberry
                        pilote ces appareils.</p>

                    <p>
                    <ul>
                        <li>Automatisation de la surveillance sur des plages programmées, modification d’images et
                            création
                            de vidéos (timelapse) en lignes de commandes
                        </li>
                        <li>Stockage des données brutes en local et en version « allégées » sur un serveur distant pour
                            visualisation externe.
                        </li>
                    </ul>
                </div>
                </p>
                <p>
                    <span class="badge badge-success">Python</span>
                    <span class="badge badge-success">PHP 7</span>
                    <span class="badge badge-success">Node.JS</span>
                    <span class="badge badge-info">Shell</span>
                    <span class="badge badge-secondary">Raspberry Pi</span>
                    <span class="badge badge-secondary">FFMPEG</span>
                </p>
                <a href="https://github.com/DarkChyper/DarkHomeServer" target="_BLANK" class="card-link">Code source
                    original</a>
                <a href="https://gitlab.com/darkhomeserver/sensor" target="_BLANK" class="card-link">Code source
                    Node.JS</a>
            </div>
        </div>

        <div class="card border-light">
            <div class="card-body">
                <a class="not-decorated collapsed" data-toggle="collapse" href="#collapseOC" role="button"
                   aria-expanded="false" aria-controls="collapseOC">
                    <h5 class="card-title"><i class="fas fa-angle-down rotate-icon"></i></span>&nbsp;10/2017 |
                        Billetterie du Louvre</h5>
                    <h6 class="card-subtitle mb-1 text-muted">Projet de parcours Openclassrooms</h6>
                </a>
                <div class="collapse" id="collapseOC">
                    <p class="card-text">Le but de ce projet était de créer une billetterie pour le musée du Louvre.</p>
                    <p>
                        Les utilisateurs pouvaient prévoir leur venue au musée en achetant les billets directement sur
                        le
                        site.<br/>
                        Le tunnel d'achat était géré par la solution Stripe.<br/>
                        Les billets étaient générés à la commande et envoyés par email, email conteant un QRcode pour
                        faciliter la vérification à l'entrée du musée.
                    </p>
                </div>
                <p>
                    <span class="badge badge-success">PHP 7</span>
                    <span class="badge badge-success">Symfony 3.3</span>
                    <span class="badge badge-warning">PHP Unit</span>
                    <span class="badge badge-secondary">Stripe</span>
                </p>
                <a href="https://github.com/DarkChyper/louvre" target="_BLANK" class="card-link">Code
                    source</a>
            </div>
        </div>

        <div class="card border-light">
            <div class="card-body">
                <a class="not-decorated collapsed" data-toggle="collapse" href="#collapseNAO" role="button"
                   aria-expanded="false" aria-controls="collapseNAO">
                    <h5 class="card-title"><i class="fas fa-angle-down rotate-icon"></i></span>&nbsp;06/2017-09/2017 |
                        Projet NAO</h5>
                    <h6 class="card-subtitle mb-1 text-muted">Projet de parcours Openclassrooms</h6>
                </a>
                <div class="collapse" id="collapseNAO">
                    <p class="card-text">Le but de ce projet était de créer une application web participative à la
                        demande
                        d'une association de défense des oiseaux (NAO pour "Nos amis les Oiseaux").</p>
                    <p>
                        Les utilisateurs pouvaient se renseigner sur les oiseaux vivant autour d'eux, saisir des
                        observations d'oiseaux qui seront validées par des naturalistes.
                    </p>
                    <p>Projet réalisé en équipe de 3 étudiants, regroupé en une société factice, la "digital science
                        factory" :
                    <ul>
                        <li>Un profil de marketing digital, designer</li>
                        <li>Un profil chef de projet et aide au développement</li>
                        <li>Et moi en tant que principal développeur et renfort designer</li>
                    </ul>
                </div>
                </p>
                <div class="collapse" id="collapseOsConcept">
                    <p>
                        <span class="badge badge-success">PHP 7</span>
                        <span class="badge badge-success">Symfony 3.3</span>
                        <span class="badge badge-warning">PHP Unit</span>
                    </p>
                </div>
                <a href="https://github.com/digitalsciencefactory/nao" target="_BLANK" class="card-link">Code
                    source</a>
            </div>
        </div>

        <div class="card border-light">
            <div class="card-body">
                <a class="not-decorated collapsed" data-toggle="collapse" href="#collapsemal" role="button"
                   aria-expanded="false" aria-controls="collapsemal">
                    <h5 class="card-title"><i class="fas fa-angle-down rotate-icon"></i></span>&nbsp;2017 | Machine à
                        laver
                        connectée</h5>
                    <h6 class="card-subtitle mb-1 text-muted">Projet personnel</h6>
                </a>
                <div class="collapse" id="collapsemal">
                    <p class="card-text">Fork d'un projet de machine à laver connectée à l'aide d'un capteur de
                        mouvements
                        et d'un module wifi
                        installés sur un raspberry pi zero. L'idée est de détecter la fin de l'essorage.</p>
                    <p>La détection d’un mouvement puis de la fin de ce mouvement déclenche l’envoi d’un message
                        horodaté.
                        j'ai complété le projet existant par l'envoie de mail en ligne de commande</p>
                    <p>Malheureusement, le manque de documentation du nombre de cycles d’essorage pour chaque programme
                        de la machine rendait l’utilisation trop hasardeuse.<br/>Finalement l’arrivée des assistants
                        vocaux avec programmation de minuteurs multiples a mis un terme au projet.
                    </p>
                </div>
                <p>
                    <span class="badge badge-success">Python</span>
                    <span class="badge badge-secondary">Raspbian</span>
                    <span class="badge badge-secondary">GPIO</span>
                </p>
                <a href="https://github.com/DarkChyper/rpi-appliance-monitor" target="_BLANK" class="card-link">Code
                    source</a>
            </div>
        </div>


        <!-- FIN PROJETS -->
    </div>
