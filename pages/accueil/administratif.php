<div id="administratif" class="row">

    <!-- PHOTO -->
    <div id="photo" class="order-2 col-6 col-sm-4 text-center">
        <img class="img-fluid photo" src="assets/img/photo.jpg" alt="une photographie de moi" />
        <p class="txt-center">Simon LHOIR<br /><small class="txt-center">Développeur</small></p>

    </div>
    <!-- FIN PHOTO -->

    <!-- CONTACT -->
    <div id="contact" class="order-3 col-6 col-sm-4">
        <h2>CONTACT</h2>
        <!-- card telephone -->
        <div class="card contact">
            <div class="row no-gutters align-items-center">
                <div class="d-none d-md-block col-sm-2 offset-2">
                    <img src="assets/img/appel-telephonique.png" class="icon" alt="icone de telephone">
                </div>
                <div class="col-md-8">
                    <div class="card-body card-contact">
                        <h5 class="card-title reduce">TELEPHONE</h5>
                        <small>06 76 93 82 60</small>
                    </div>
                </div>
            </div>
        </div>
        <!-- card adresse -->
        <div class="card contact mb-2">
            <div class="row no-gutters align-items-center">
                <div class="d-none d-md-block col-sm-2 offset-2">
                    <img src="assets/img/broche-de-localisation.png" class="icon" alt="icone de localisation">
                </div>
                <div class="col-md-8">
                    <div class="card-body card-contact">
                        <h5 class="card-title reduce">ADRESSE</h5>
                        <small>62800 Liévin</small>
                    </div>
                </div>
            </div>
        </div>
        <!-- card email -->
        <div class="card contact mb-2">
            <div class="row no-gutters align-items-center">
                <div class="d-none d-md-block col-sm-2 offset-2">
                    <img src="assets/img/enveloppe.png" class="icon" alt="icone d'une enveloppe">
                </div>
                <div class="col-md-8">
                    <div class="card-body card-contact">
                        <h5 class="card-title reduce">EMAIL</h5>
                        <small><a href="mailto:pro@lhoir.me">pro@lhoir.me</a></small>
                    </div>
                </div>
            </div>
        </div>
        <!-- card network -->
        <div class="card contact mb-2">
            <div class="row no-gutters align-items-center">
                <div class="d-none d-md-block col-sm-2 offset-2">
                    <img src="assets/img/monde.png" class="icon" alt="icone du monde">
                </div>
                <div class="col-md-8">
                    <div class="card-body card-contact">
                        <h5 class="card-title reduce">RESEAUX</h5>
                        <small class="text-muted"><a href="http://linkedin.simon-lhoir.fr" target="_BLANK">linkedin.simon-lhoir.fr</a></small><br />
                        <small class="text-muted"><a href="https://darkblog.lhoir.me" target="_BLANK">darkblog.lhoir.me</a></small>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- FIN CONTACT -->

    <!-- A PROPOS -->
    <div id="a_propos" class="order-12 order-sm-1 col-12 col-sm-4">
        <h2>A PROPOS</h2>
        <p>Passionn&eacute; d’informatique et de nouvelles technologies, je me d&eacute;finis comme un geek  curieux et  volontaire.<br />
            Peu importe la technologie ou la technique utilis&eacute;e, mon but est de fournir des solutions robustes, maintenables et qui respectent les normes de qualit&eacute;.</p>
    </div>
    <!-- FIN A PROPOS -->

</div>
