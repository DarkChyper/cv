<div class="row">

    <div class="card mb-3">
        <div class="row no-gutter">
            <div class="d-none d-sm-none d-md-block col-md-12 col-xl-4">
                <img src="./assets/img/oc-cdp/lauren-mancke-aOC7TSLb1o8-unsplash.jpg" class="card-img"
                     alt="Illustration d'un pc portable par Kari Shea - https://unsplash.com/@karishea"/>
            </div>
            <div class="col-12 col-md-12 col-xl-8">
                <div class="card-body">
                    <h2 class="card-title">Chef de projet Multimédia - Développement</h2>
                    <p class="card-text">De septembre 2016 à octobre 2017, j'ai suivi, hors temps de travail, le
                        parcours "Chef de projet Multimédia,
                        option développement, de chez <a href="https://www.openclassrooms.com/" target="_blank">Openclassrooms</a>.
                    </p>
                    <p class="card-text">Ce parcours composé de 6 projets tutorés n'est plus disponible aujourd'hui, je
                        souhaite donc partager les projets réalisés sur cette
                        page.</p>
                </div>
            </div>
        </div>
    </div>
    <div class="col-12">
        <div class="accordion" id="accordionCDP">
            <div class="card">
                <div class="card-header" id="headingOne">
                    <h2 class="mb-0">
                        <button class="btn btn-link btn-block text-left" type="button" data-toggle="collapse"
                                data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                            Créer et déployer un site en ligne
                        </button>
                    </h2>
                </div>
                <div id="collapseOne" class="collapse" aria-labelledby="headingOne" data-parent="#accordionCDP">
                    <div class="card-body">
                        <strong>Objectif :</strong> Réaliser un site web pour la ville de son choix afin d'amener des
                        informations aux habitants.<br/>
                        Site réalisé avec wordpress.
                        <p>
                            <span class="badge badge-success">PHP</span>
                            <span class="badge badge-primary">HTML/CSS</span>
                            <span class="badge badge-secondary">Wordpress</span>
                            <span class="badge badge-secondary">Documentation</span>
                        </p>
                        <p><a href="https://youtu.be/9jSLLapCbtI" target="_blank">Vid&eacute;o de la soutenance</a></p>
                    </div>
                </div>
            </div>

            <div class="card">
                <div class="card-header" id="headingTwo">
                    <h2 class="mb-0">
                        <button class="btn btn-link btn-block text-left collapsed" type="button" data-toggle="collapse"
                                data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                            Initier et lancer un projet Multimédia
                        </button>
                    </h2>
                </div>
                <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionCDP">
                    <div class="card-body">
                        <strong>Objectif :</strong> Créer une proposition commerciale, défendre son projet.
                        Créer le cahier des charges, le budget prévisionnel, le planning de réalisation du site d'un
                        évènement commercial.
                        <p>
                            <span class="badge badge-secondary">Proposition commerciale</span>
                            <span class="badge badge-secondary">Cahier des charges</span>
                            <span class="badge badge-secondary">Budget Prévisionnel</span>
                            <span class="badge badge-secondary">Planning</span>
                        </p>
                        <p><a href="https://youtu.be/iWnPh4eEE3U" target="_blank">Vid&eacute;o de la soutenance</a></p>
                        <h3>Remarques de l'évaluateur</h3>
                        <blockquote class="blockquote">
                            <p class="mb-0">
                                <ul>
                                    <li>Projet convaincant</li>
                                    <li>Simon a des capacités certaines de vulgarisation; utiles vis à vis de certains décideurs.</li>
                                    <li>Il explique la technique par rapport à ce qu'elle apporte</li>
                                    <li>Il lui manque un peu de synthèse niveau "executive summary"</li>
                                    <li>Il lui reste à travailler l'exercice de présentation (durée, scénario...) dans ces futures présentations</li>
                                    <li>Faire des choix. </li>
                                    <li>Quelques imprécisions (cycle en V, versioning du CDC) ; vocabulaire "Fullstack"</li>
                                    <li>La formation chef de projet l'intéresse vraiment; et je pense qu'il a un réel potentiel</li>
                                    <li>Conseil de suivre l'évolution des cours gestProj sur OC</li>
                                </ul>
                            </p>
                            
                        </blockquote>
                    </div>
                </div>
            </div>

            <div class="card">
                <div class="card-header" id="headingThree">
                    <h2 class="mb-0">
                        <button class="btn btn-link btn-block text-left collapsed" type="button" data-toggle="collapse"
                                data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                            Créer l'application d'un site éducatif
                        </button>
                    </h2>
                </div>
                <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordionCDP">
                    <div class="card-body">
                        <strong>Objectif :</strong> Créer une application iOS compagnon pour le site d'éducation aux
                        langues étrangères Multilingua.
                        <p>
                            <span class="badge badge-success">Swift</span>
                            <span class="badge badge-secondary">iOS</span>
                        </p>
                        <p><a href="https://youtu.be/sDoaOoRrIUA" target="_blank">Vid&eacute;o de la soutenance</a></p>
                        <h3>Remarques de l'évaluateur</h3>
                        <blockquote class="blockquote">
                            <p class="mb-0">
                                Projet réalisé en iOS. Bonne soutenance et bien présentée. Il y a eu un effort graphique, le projet est complet et le cahier des charges respecté. L'élève a su répondre à toutes mes questions et arrive bien à se projeter pour une V2.
                            </p>
                        </blockquote>
                    </div>
                </div>
            </div>

            <div class="card">
                <div class="card-header" id="heading4">
                    <h2 class="mb-0">
                        <button class="btn btn-link btn-block text-left collapsed" type="button" data-toggle="collapse"
                                data-target="#collapse4" aria-expanded="false" aria-controls="collapse4">
                            Développer un back-end pour un client
                        </button>
                    </h2>
                </div>
                <div id="collapse4" class="collapse" aria-labelledby="heading4" data-parent="#accordionCDP">
                    <div class="card-body">
                        <strong>Objectif :</strong> Créer la billetterie du musée du Louvre en responsive.
                        <p>
                            <span class="badge badge-success">PHP 7</span>
                            <span class="badge badge-success">Symfony 3.3</span>
                            <span class="badge badge-warning">PHPunit</span>
                            <span class="badge badge-warning">Sensiolabs</span>
                        </p>
                        <p><a href="https://youtu.be/quA3fVl93To" target="_blank">Vid&eacute;o de la soutenance</a></p>
                        <h3>Remarques de l'évaluateur</h3>
                        <blockquote class="blockquote">
                            <p class="mb-0">
                                Très bonne soutenance, dynamique et bien menée.<br>
                                Du coté technique, le projet va bien au delà des attentes. Simon a acquis un niveau avancé sur Symfony, que ce soit sur l'organisation, le suivi des bonnes pratiques, ou l'utilisation de techniques poussées parfaitement adaptées à la problématique initiale (services, validateurs, listeners, tests, etc). L'ensemble est très professionnel.
                            </p>
                        </blockquote>
                    </div>
                </div>
            </div>

            <div class="card">
                <div class="card-header" id="heading5">
                    <h2 class="mb-0">
                        <button class="btn btn-link btn-block text-left collapsed" type="button" data-toggle="collapse"
                                data-target="#collapse5" aria-expanded="false" aria-controls="collapse5">
                            Créer une agence digitale en équipe
                        </button>
                    </h2>
                </div>
                <div id="collapse5" class="collapse" aria-labelledby="heading5" data-parent="#accordionCDP">
                    <div class="card-body">
                        <strong>Objectif :</strong> Réaliser une agence digitale et répondre au besoin d'une association
                        (fictive). Mélange de profils Marketing, Designer et Développeur.
                        <p>
                            <span class="badge badge-success">PHP 7</span>
                            <span class="badge badge-success">Symfony 3.3</span>
                            <span class="badge badge-primary">Creation de logo</span>
                            <span class="badge badge-warning">PHPunit</span>
                        </p>
                        <p><a href="https://youtu.be/1maghltR4_w" target="_blank">Vid&eacute;o de la soutenance</a></p>
                        <h3>Remarques de l'évaluateur</h3>
                        <blockquote class="blockquote">
                            <p class="mb-0">
                                <p>Projet validé.</p>

                                <p>Un très bon projet qui répond complètement aux attentes du client, côté présentation, Simon maîtrise son sujet, la présentation gagnerait à être plus "naturelle" et moins tournée vers les slides (bien que cet aspect soit tout à fait correct) avec une présentation de l'application directement.</p>

                                <p>Côté GDP, Simon a su s'intégrer au mieux au sein de l'équipe et remplir les objectifs.</p>
                            </p>
                        </blockquote>
                    </div>
                </div>
            </div>

            <div class="card">
                <div class="card-header" id="heading6">
                    <h2 class="mb-0">
                        <button class="btn btn-link btn-block text-left collapsed" type="button" data-toggle="collapse"
                                data-target="#collapse6" aria-expanded="false" aria-controls="collapse6">
                            Contribuer à l'écosystème
                        </button>
                    </h2>
                </div>
                <div id="collapse6" class="collapse" aria-labelledby="heading6" data-parent="#accordionCDP">
                    <div class="card-body">
                        <strong>Objectif :</strong> Ecriture d'un tutoriel, à destination des autres étudiants, afin de
                        déployer correctement une application PHP/Symfony sur un serveur virtuel OVH.
                        <p>
                            <span class="badge badge-success">PHP 7</span>
                            <span class="badge badge-success">Symfony 3.3</span>
                            <span class="badge badge-warning">Apache 2</span>
                            <span class="badge badge-warning">Virtual host</span>
                            <span class="badge badge-secondary">Documentation</span>
                        </p>
                        <p><a href="https://youtu.be/bnvwvVmvCVc" target="_blank">Vid&eacute;o de la soutenance</a></p>
                        <p>Aperçu du cours - Bientôt</p>
                        <h3>Remarques de l'évaluateur</h3>
                        <blockquote class="blockquote">
                            <p class="mb-0">
                                <p>Sujet choisi  : le cours</p>

                                <p>Un projet parfaitement en phase avec le niveau d'attentes par ailleurs consolidé par une soutenance claire et fluide. Globalement du très bon travail.</p>

                                <p>url du cours dans CourseLab : <br />
                                    <a href="https://openclassrooms.com/courses/4663211?status=waiting-for-publication" target="_blank">https://openclassrooms.com/courses/4663211?status=waiting-for-publication</a> - visible uniquement si connecté à OC</p>
                            </p>
                        </blockquote>

                    </div>
                </div>
            </div>

        </div>
    </div>
</div>
