<nav class="navbar fixed-bottom navbar-dark justify-content-center" id="bottom-nav">
    <ul class="nav ">
        <li class="nav-item">
            <a class="nav-link active" href="https://gitlab.com/DarkChyper/cv"  target="_BLANK">2024-03-01</a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="assets/doc/cv-slhoir.pdf"  target="_BLANK">CV imprimable</a>
        </li>
    </ul>
</nav>
